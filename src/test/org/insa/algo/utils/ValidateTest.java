package org.insa.algo.utils;


import static org.junit.Assert.assertEquals;


import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.AStarAlgorithm;
import org.insa.algo.shortestpath.BellmanFordAlgorithm;
import org.insa.algo.shortestpath.DijkstraAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.algo.shortestpath.ShortestPathSolution;
import org.insa.graph.Graph;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.junit.BeforeClass;
import org.junit.Test;

public class ValidateTest {

	static GraphReader reader;
	static Graph graph;
	static ArrayList<int[]> points = new ArrayList<int[]>();
	static int echantSize = 100;
 
    @BeforeClass
    public static void initAll() throws IOException {
    	// Create a graph reader.
        String mapName = "/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/midi-pyrenees.mapgr";

        reader = new BinaryGraphReader(
                new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
        // Read the graph.
        graph = reader.read();
        
        for (int i = 0; i<echantSize ; i++)
        {
        	int randomDep = ThreadLocalRandom.current().nextInt(0, graph.getNodes().size());
        	int randomArr = ThreadLocalRandom.current().nextInt(0, graph.getNodes().size());
        	if (graph.get(randomDep).getPoint().distanceTo(graph.get(randomArr).getPoint())<5000) 
        	{
        		i-=1;
        	}
        	else {
        		int[] list = {randomDep,randomArr};
        		points.add(list);
        	}
        	
        }
                
    }

    @Test
    public void testAlgo() {
    	ArrayList<ArrayList<Float>> results = new ArrayList<ArrayList<Float>>(); //Arraylist d'arraylist des résultats

    	ShortestPathData spd;
    	DijkstraAlgorithm da;
    	AStarAlgorithm asa;
    	ShortestPathSolution sps;
    	ShortestPathSolution spsr;
    	int i = 0; //Compteur
    	int cpt = 0; //compteur

    	
    	for (int[] d : points) //On parcourt la liste des points initialisés
    	{
    		results.add(new ArrayList<Float>()); //On créé la liste des résultats
    		spd = new ShortestPathData(graph, graph.get(d[0]), graph.get(d[1]), ArcInspectorFactory.getAllFilters().get(0)); //On initialise le ShortestPathData
    		da = new DijkstraAlgorithm(spd); //Initialisation Djikstra
    		asa = new AStarAlgorithm(spd); //Initialisation A*
    		sps = da.run();
    		spsr = asa.run();
    		
    		if (sps.isFeasible()) //Si faisable via Djikstra
    		{
    			results.get(i).add(sps.getPath().getLength()); //On ajoute la distance à l'array des résultats
    			results.get(i).add((float)sps.getPath().getMinimumTravelTime()); //On ajoute le temps de transfert
    		}
    		else { //Sinon on ajoute 0
    			results.get(i).add((float)0);
    			results.get(i).add((float)0);
    		}
    		if (spsr.isFeasible()) //idem
    		{
    			results.get(i).add(spsr.getPath().getLength());
        		results.get(i).add((float)spsr.getPath().getMinimumTravelTime());
    		}
    		else
    		{
    			results.get(i).add((float)0);
    			results.get(i).add((float)0);
    		}
    		i++;
    		
        	System.out.println(i);
    	}
    	
    	i = 0;
    	
    	//Parcours des résultats
    	for (ArrayList<Float> d: results)
    	{
    		//Si les résultats de Djikstra et A* sont égaux (3% de marge)

    		if (((Math.abs(d.get(0).doubleValue() - d.get(2).doubleValue())/d.get(0).doubleValue() < 3) && (Math.abs(d.get(1).doubleValue() - d.get(3).doubleValue())/d.get(1).doubleValue() < 3)) || (d.get(0).doubleValue()==0 && d.get(1).doubleValue()==0 && d.get(2).doubleValue() == 0 && d.get(3).doubleValue() == 0))
    		{
    			cpt++;

    		}
    		else {
    			System.out.println(d);
    		}

    		i++;
    	}
    	
    	//Affichage des résultats
    	System.out.println("Taille de l'échantillon : " + echantSize);
    	System.out.println("Taux d'erreurs: " + (1-(cpt/this.echantSize)));


    }
}
