package org.insa.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.BellmanFordAlgorithm;
import org.insa.algo.shortestpath.DijkstraAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.algo.shortestpath.ShortestPathSolution;
import org.insa.graph.RoadInformation.RoadType;
import org.junit.BeforeClass;
import org.junit.Test;

public class DijkstraTest2 {
	public enum Eval {
		Time,
		Distance;
	}

    // Small graph use for tests
    private static Graph graph;

    // Origins
    private static Node origin;
    
    // Destination
    private static Node destination;
    
    // Eval
    private Eval eval;
    

    @BeforeClass
    public static void initAll() throws IOException {
    		//Charger le graphe
    		//Mettre l'origine
    		//Mettre la destination
    		//choisir le mode d'eval
    }


	@Test
    public void testDijkstra() {
		//Rajouter la selection du mode d'éval
		//Modifier pour tester quelques scénarios sur le graph (pas tout le graphe, ca va prendre des plombs).
		
    	int i,j;
    	int cpt = 0, cptr = 0;
    	
		for(i = 0 ; i < graph.size() ; i++) {			
			for(j = 0 ; j < graph.size(); j++) {
				
				ShortestPathData spd = new ShortestPathData(graph, graph.get(i), graph.get(j), ArcInspectorFactory.getAllFilters().get(0));
				DijkstraAlgorithm da = new DijkstraAlgorithm(spd);
				BellmanFordAlgorithm bfa = new BellmanFordAlgorithm(spd);
				ShortestPathSolution sps = da.run();
				ShortestPathSolution spsr = bfa.run();
				if (sps.isFeasible()) {
					assertEquals((long)sps.getPath().getLength(), (long)spsr.getPath().getLength());
					cpt ++;
				}
				if (spsr.isFeasible()) {
					cptr ++;
				}
			}
		}
		
		assertEquals(cpt, cptr);
    	
    }
    

}

