package org.insa.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.BellmanFordAlgorithm;
import org.insa.algo.shortestpath.DijkstraAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.algo.shortestpath.ShortestPathSolution;
import org.insa.graph.RoadInformation.RoadType;
import org.junit.BeforeClass;
import org.junit.Test;

public class DijkstraTest {

    // Small graph use for tests
    private static Graph graph;

    // List of nodes
    private static Node[] nodes;

    // List of arcs in the graph, a2b is the arc from node A (0) to B (1).
    @SuppressWarnings("unused")
    private static Arc p1a2, p1a3, p2a4, p2a5, p2a6, p3a1, p3a2, p3a6, p5a4, p5a3, p5a6, p6a5;

    // Some paths...
    private static Path path;

    @BeforeClass
    public static void initAll() throws IOException {

        // 10 and 20 meters per seconds
        RoadInformation speed10 = new RoadInformation(RoadType.MOTORWAY, null, true, 36, "");
        
        // Create nodes
        nodes = new Node[6];
        for (int i = 0; i < nodes.length; ++i) {
            nodes[i] = new Node(i, null);
        }

        // Add arcs...
        p1a2 = Node.linkNodes(nodes[0], nodes[1], 7, speed10, null);
        p1a3 = Node.linkNodes(nodes[0], nodes[2], 8, speed10, null);
        p2a4 = Node.linkNodes(nodes[1], nodes[3], 4, speed10, null);
        p2a5 = Node.linkNodes(nodes[1], nodes[4], 1, speed10, null);
        p2a6 = Node.linkNodes(nodes[1], nodes[5], 5, speed10, null);
        p3a1 = Node.linkNodes(nodes[2], nodes[0], 7, speed10, null);
        p3a2 = Node.linkNodes(nodes[2], nodes[1], 2, speed10, null);
        p3a6 = Node.linkNodes(nodes[2], nodes[5], 2, speed10, null);
        p5a4 = Node.linkNodes(nodes[4], nodes[3], 2, speed10, null);
        p5a3 = Node.linkNodes(nodes[4], nodes[2], 2, speed10, null);
        p5a6 = Node.linkNodes(nodes[4], nodes[5], 3, speed10, null);
        p6a5 = Node.linkNodes(nodes[5], nodes[4], 3, speed10, null);


        graph = new Graph("ID", "", Arrays.asList(nodes), null);

        path = new Path(graph, new ArrayList<Arc>());

    }


	@Test
    public void testDijkstra() {
    	int i,j;
    	int cpt = 0, cptr = 0;
    	
		for(i = 0 ; i < graph.size() ; i++) {			
			for(j = 0 ; j < graph.size(); j++) {
				
				ShortestPathData spd = new ShortestPathData(graph, nodes[i], nodes[j], ArcInspectorFactory.getAllFilters().get(0));
				DijkstraAlgorithm da = new DijkstraAlgorithm(spd);
				BellmanFordAlgorithm bfa = new BellmanFordAlgorithm(spd);
				ShortestPathSolution sps = da.run();
				ShortestPathSolution spsr = bfa.run();
				if (sps.isFeasible()) {
					assertEquals((long)sps.getPath().getLength(), (long)spsr.getPath().getLength());
					cpt ++;
				}
				if (spsr.isFeasible()) {
					cptr ++;
				}
			}
		}
		
		assertEquals(cpt, cptr);

    	
    }

}
