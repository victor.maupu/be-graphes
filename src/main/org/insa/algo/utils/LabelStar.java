package org.insa.algo.utils;
import org.insa.graph.Node;

public class LabelStar extends Label implements Comparable<Label> {
	/*
	 * Attributs :
	 */
	private double borneInf;	
	
	/*
	 * Constructeurs :
	 */

	public LabelStar(double etiquette, double bI, Node n) {
		super(etiquette, n);
		this.borneInf = bI;
	}
	
	/*
	 * Méthode :
	 */
	
	@Override
	public double getCost() {
		return this.borneInf + this.etiquette;
	}
	
	public void setBorneInf(double val) {
		this.borneInf = val;
	}
	
	public double getBorneInf() {
		return this.borneInf;
	}
}