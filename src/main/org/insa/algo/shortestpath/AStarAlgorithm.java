package org.insa.algo.shortestpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.insa.algo.AbstractInputData.Mode;
import org.insa.algo.AbstractSolution.Status;
import org.insa.algo.utils.*;
import org.insa.graph.Arc;
import org.insa.graph.Node;
import org.insa.graph.Path;

public class AStarAlgorithm extends DijkstraAlgorithm{

	public AStarAlgorithm(ShortestPathData data)
	{
		super(data);
	}
	
	protected ShortestPathSolution doRun() {
		ShortestPathData data = getInputData();
		ShortestPathSolution solution = null;
        int i = 0;
        Label LabelMin;
        ArrayList<Arc> listArcs = new ArrayList<Arc>();
        double coeffMode = 1;
        
        //Initialisations
        HashMap<Integer,Node> locked = new HashMap<Integer,Node>();
        BinaryHeap<Label> tas = new BinaryHeap<Label>();
        if (this.getInputData().getMode().equals(Mode.TIME))
        {
        	//estimation du minimum de temps est le minimum de la distance divisé par le maximum de vitesse
        	coeffMode = 1/data.getGraph().getGraphInformation().getMaximumSpeed();
        }
       for (Node n : data.getGraph())
       {
    	   new LabelStar(Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY, n);
       }

       LabelStar.getLabel(data.getOrigin().getId()).setEtiquette(0);
       tas.insert(LabelStar.getLabel(data.getOrigin().getId())); 
       notifyOriginProcessed(data.getOrigin());
        //Traitement
        while ((!locked.containsKey(data.getDestination().getId())) && !tas.isEmpty())
        {
        	LabelMin = tas.deleteMin();
        	locked.put(LabelMin.getNode().getId(),LabelMin.getNode());
        	notifyNodeMarked(LabelMin.getNode());

        	for (Arc a : LabelMin.getNode())
        	{

				// Small test to check allowed roads...
				if (!data.isAllowed(a)) {
					continue;
				}
				
        		if (LabelMin.getEtiquette()+data.getCost(a)<LabelStar.getLabel(a.getDestination()).getEtiquette())
        		{
        			if (LabelStar.getLabel(a.getDestination()).getEtiquette() != Double.POSITIVE_INFINITY && !locked.containsKey(a.getDestination().getId()))
        			{
        				tas.remove(LabelStar.getLabel(a.getDestination()));
        			}
        			else
        			{
        				notifyNodeReached(a.getDestination());
        			}
        			Label.getLabel(a.getDestination()).setEtiquette(LabelMin.getEtiquette()+data.getCost(a));
        			((LabelStar)LabelStar.getLabel(a.getDestination())).setBorneInf(coeffMode*a.getDestination().getPoint().distanceTo(data.getDestination().getPoint()));
        			LabelStar.getLabel(a.getDestination()).setPrevNode(a.getOrigin());
        			LabelStar.getLabel(a.getDestination()).setPrevArc(a);
        			
        			tas.insert(LabelStar.getLabel(a.getDestination()));
        		}
        	}
        	i++;
        }
        if (locked.containsKey(data.getDestination().getId()))
        {
        	notifyDestinationReached(data.getDestination());
        }
        Node n = data.getDestination();
        while (LabelStar.getLabel(n).getPrevNode() != null)
        {
        	listArcs.add(LabelStar.getLabel(n).getPrevArc());
        	n = LabelStar.getLabel(n).getPrevNode();
        }
        Collections.reverse(listArcs);
        
        if(listArcs.size()== 0)
        {
        	solution = new ShortestPathSolution(data, Status.INFEASIBLE);
        }
        else
        {
        	solution = new ShortestPathSolution(data, Status.OPTIMAL, new Path(data.getGraph(),listArcs));
        }
        
        //System.out.println("Nombre d'arcs : " + listArcs.size()+"\n Nombre d'itérations : " + i);
        return solution;
	}
}
